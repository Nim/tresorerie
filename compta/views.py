from django.views.generic import ListView, CreateView, DeleteView, UpdateView

from .models import Treso


class Home(ListView):
    model = Treso


class Add(CreateView):
    model = Treso


class Update(UpdateView):
    model = Treso


class Delete(DeleteView):
    model = Treso
