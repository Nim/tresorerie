# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('compta', '0002_auto_20141104_1554'),
    ]

    operations = [
        migrations.AlterField(
            model_name='treso',
            name='b5',
            field=models.PositiveIntegerField(verbose_name='billets de 5€', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='treso',
            name='b500',
            field=models.PositiveIntegerField(verbose_name='billets de 500€', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='treso',
            name='p1',
            field=models.PositiveIntegerField(verbose_name='pièces de 1€', default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='treso',
            name='p2',
            field=models.PositiveIntegerField(verbose_name='pièces de 2€', default=0),
            preserve_default=True,
        ),
    ]
