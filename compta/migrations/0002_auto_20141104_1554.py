# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('compta', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='treso',
            name='b5',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='treso',
            name='b500',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='treso',
            name='p1',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='treso',
            name='p2',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
