# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Treso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('p1', models.IntegerField()),
                ('p2', models.IntegerField()),
                ('b5', models.IntegerField()),
                ('b500', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
