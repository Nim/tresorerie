from django.conf.urls import patterns, include, url
from django.contrib import admin

from .views import Home, Add, Update, Delete

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', Home.as_view(), name='home'),
    url(r'^add$', Add.as_view(), name='add'),
    url(r'^update/(?P<pk>\d+)$', Update.as_view(), name='update'),
    url(r'^delete/(?P<pk>\d+)$', Delete.as_view(), name='delete'),
)
