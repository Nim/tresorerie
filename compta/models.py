from django.db.models import PositiveIntegerField, Model, DateTimeField
from django.core.urlresolvers import reverse

class Treso(Model):
    date = DateTimeField(auto_now_add=True)
    p1 = PositiveIntegerField('pièces de 1€', default=0)
    p2 = PositiveIntegerField('pièces de 2€', default=0)
    b5 = PositiveIntegerField('billets de 5€', default=0)
    b500 = PositiveIntegerField('billets de 500€', default=0)

    def get_absolute_url(self):
        return reverse('home')

    def total(self):
        return self.p1 + 2 * self.p2 + 5 * self.b5 + 500 * self.b500
